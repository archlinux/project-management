#!/bin/bash
set -o nounset -o errexit -o pipefail

gitlab_api() {
  curl -sSf \
    --request "${1}" \
    --header "PRIVATE-TOKEN: ${GITLAB_PROJECT_TOKEN}" \
    "https://gitlab.archlinux.org/api/v4/${2}" "${@:3}"
}

create_issue() {
  local project="${1}"
  local title="${2}"
  local description="${3}"

  gitlab_api POST "projects/${project}/issues" --data-urlencode "title=${title}" --data-urlencode "description=$(echo -e "${description}")"
}

create_monthly_report_issue() {
  local project month month_name year title description
  project="archlinux%2Fproject-management"
  month="$(date +%m)"
  month_name="$(LC_ALL=C date +%B)"
  year="$(date +%Y)"
  title="Monthly Report for ${month_name} ${year}"
  description="/label ~monthly-report\nPad for ${month_name} ${year}: https://md.archlinux.org/${year}-${month}_monthly-report"

  create_issue "${project}" "${title}" "${description}"
}

create_monthly_report_issue
